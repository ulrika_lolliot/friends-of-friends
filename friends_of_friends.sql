-- SQL query to get the friends of friends, by Ulrika Lolliot

-- Instructions: 
-- 1 - get the friends of friends of a user    
-- 2 - sort them by number of common connexions (friends of friends who share most friends with the users)

-- Solution

-- we use a table that associates two user id
  
CREATE TABLE IF NOT EXISTS `t` (
  `u_from` int(3) NOT NULL,
  `u_to` int(3) NOT NULL,
  PRIMARY KEY (`u_from`,`u_to`)
);

-- demo data in order to test

INSERT INTO `t` (`u_from`, `u_to`) VALUES
(123, 231),
(123, 234),
(231, 318),
(231, 321),
(233, 123),
(233, 321),
(233, 387),
(234, 231),
(234, 321),
(234, 341),
(456, 234),
(234, 123),
(434, 123),
(456, 434);

-- If we take into account all directions and mutual friends, the friend of friend can be associated to a particular user according to 2 x 2 = 4 possibilities:

-- A -> B -> C
-- Wich correspond to the request (to get friends of friends of user id = 123):
SELECT t2.u_to FROM t AS t1 INNER JOIN t AS t2 ON t1.u_to = t2.u_from WHERE t1.u_from = 123; 
-- One of our result would be, for ex.: 
-- t1.u_from = 123
-- t1.u_to = 231
-- t2.u_from = 231
-- t2.u_to = 318
-- so we have indeed: 123 -> 231 -> 318

-- A -> B <- C
-- Wich correspond to the request:
SELECT t2.u_from FROM t AS t1 INNER JOIN t AS t2 ON t1.u_to = t2.u_to WHERE t1.u_from = 123 AND t2.u_from <> 123; 
-- One of our result would be, for ex.: 
-- t1.u_from = 123
-- t1.u_to = 231
-- t2.u_from = 234
-- t2.u_to = 231
-- so we have indeed: 123 -> 231 <- 234

-- A <- B -> C
-- Wich correspond to the request:
SELECT t2.u_to FROM t AS t1 INNER JOIN t AS t2 ON t1.u_from = t2.u_from WHERE t1.u_to = 123 AND t2.u_to <> 123;      
-- One of our result would be, for ex.: 
-- t1.u_from = 233
-- t1.u_to = 123
-- t2.u_from = 233
-- t2.u_to = 387
-- so we have indeed: 123 <- 233 -> 387
                      
-- A <- B <- C
-- Wich correspond to the request:
SELECT t2.u_from FROM t AS t1 INNER JOIN t AS t2 ON t1.u_from = t2.u_to WHERE t1.u_to = 123;     
-- One of our result would be, for ex.: 
-- t1.u_from = 434
-- t1.u_to = 123
-- t2.u_from = 456
-- t2.u_to = 434
-- so we have indeed: 123 <- 434 <- 456

-- So, to get all the friends of friends of 123:

SELECT t2.u_to AS friend_of_friend FROM t AS t1 
INNER JOIN t AS t2 ON t1.u_to = t2.u_from       
WHERE t1.u_from = 123 
UNION 
SELECT t2.u_from AS friend_of_friend FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_to = t2.u_to 
WHERE t1.u_from = 123 AND t2.u_from <> 123
UNION 
SELECT t2.u_to AS friend_of_friend FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_from = t2.u_from  
WHERE t1.u_to = 123 AND t2.u_to <> 123
UNION 
SELECT t2.u_from AS friend_of_friend FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_from = t2.u_to  
WHERE t1.u_to = 123;

-- The above query is enough if we make sure that we don't have mirror records of that kind 
-- when inserting in "user" table: 
-- user_from  user_to
-- A          B
-- B          A
-- But in our case we don't (for ex, we have 123 -> 234 and 234 -> 123) 
-- so we need to take this into account in the SQL query:

SELECT t2.u_to AS friend_of_friend FROM t AS t1
INNER JOIN t AS t2 ON t1.u_to = t2.u_from       
WHERE t1.u_from = 123 AND t2.u_to <> 123
UNION 
SELECT t2.u_from AS friend_of_friend FROM t AS t1     
INNER JOIN t AS t2 ON t1.u_to = t2.u_to 
WHERE t1.u_from = 123 AND t2.u_from <> 123
UNION 
SELECT t2.u_to AS friend_of_friend FROM t AS t1  
INNER JOIN t AS t2 ON t1.u_from = t2.u_from  
WHERE t1.u_to = 123 AND t2.u_to <> 123
UNION 
SELECT t2.u_from AS friend_of_friend FROM t AS t1    
INNER JOIN t AS t2 ON t1.u_from = t2.u_to  
WHERE t1.u_to = 123 AND t2.u_from <> 123;

-- remove those who are already 123's friends

SELECT t2.u_to AS friend_of_friend FROM t AS t1 
INNER JOIN t AS t2 ON t1.u_to = t2.u_from       
WHERE t1.u_from = 123 AND t2.u_to <> 123 
AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)
UNION 
SELECT t2.u_from AS friend_of_friend FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_to = t2.u_to 
WHERE t1.u_from = 123 AND t2.u_from <> 123 
AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)
UNION 
SELECT t2.u_to AS friend_of_friend FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_from = t2.u_from  
WHERE t1.u_to = 123 AND t2.u_to <> 123 
AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)
UNION 
SELECT t2.u_from AS friend_of_friend FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_from = t2.u_to  
WHERE t1.u_to = 123 AND t2.u_from <> 123 
AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123);

-- Now we have answered to question 1 : get all the friends of friends of a user
-- that are not already directly the user's friends.
-- In our example data, those are: 318, 321, 341, 456 and 387

-- To answer question 2, we need to get also the doubles, in order to quantify the common connexions
-- so we will use UNION ALL instead
-- and we add - temporarily - some columns to see better what's going on

SELECT t1.u_from AS source_from, t1.u_to AS source_to, t2.u_from AS target_from, t2.u_to AS target_to, 'target_to' AS friend_of_friend
FROM t AS t1 
INNER JOIN t AS t2 ON t1.u_to = t2.u_from       
WHERE t1.u_from = 123 AND t2.u_to <> 123 
AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

UNION ALL
SELECT t1.u_from AS source_from, t1.u_to AS source_to, t2.u_from AS target_from, t2.u_to AS target_to, 'target_from' AS friend_of_friend
FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_to = t2.u_to 
WHERE t1.u_from = 123 AND t2.u_from <> 123 
AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

UNION ALL
SELECT t1.u_from AS source_from, t1.u_to AS source_to, t2.u_from AS target_from, t2.u_to AS target_to, 'target_to' AS friend_of_friend
FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_from = t2.u_from  
WHERE t1.u_to = 123 AND t2.u_to <> 123 
AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

UNION ALL
SELECT t1.u_from AS source_from, t1.u_to AS source_to, t2.u_from AS target_from, t2.u_to AS target_to, 'target_from' AS friend_of_friend
FROM t AS t1   
INNER JOIN t AS t2 ON t1.u_from = t2.u_to  
WHERE t1.u_to = 123 AND t2.u_from <> 123 
AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123);

-- Now we have all the friends of friends of a user (that are not also direct friends of the user)
-- with doubles (because there can be different common connexions which we'll count), 
-- But there are also doubles in the common connexions of these friends of friends, 
-- and we don't want that, so we'll take it into account with a GROUP BY statement 

SELECT friend, friend_of_friend FROM  
( 
	SELECT t2.u_from AS friend, t2.u_to AS friend_of_friend
	FROM t AS t1 
	INNER JOIN t AS t2 ON t1.u_to = t2.u_from       
	WHERE t1.u_from = 123 AND t2.u_to <> 123 
	AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

	UNION ALL
	SELECT t2.u_to AS friend, t2.u_from AS friend_of_friend
	FROM t AS t1   
	INNER JOIN t AS t2 ON t1.u_to = t2.u_to 
	WHERE t1.u_from = 123 AND t2.u_from <> 123 
	AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

	UNION ALL
	SELECT t2.u_from AS friend, t2.u_to AS friend_of_friend
	FROM t AS t1   
	INNER JOIN t AS t2 ON t1.u_from = t2.u_from  
	WHERE t1.u_to = 123 AND t2.u_to <> 123 
	AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

	UNION ALL
	SELECT t2.u_to AS friend, t2.u_from AS friend_of_friend
	FROM t AS t1   
	INNER JOIN t AS t2 ON t1.u_from = t2.u_to  
	WHERE t1.u_to = 123 AND t2.u_from <> 123 
	AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123) 
) AS top_table
GROUP BY friend, friend_of_friend;

-- now we just need to group by friend_of_friend, count the common connexions and sort accordingly:

SELECT friend_of_friend, count(friend) AS common_connexions FROM 
(
	SELECT friend, friend_of_friend FROM  
	( 
		SELECT t2.u_from AS friend, t2.u_to AS friend_of_friend
		FROM t AS t1 
		INNER JOIN t AS t2 ON t1.u_to = t2.u_from       
		WHERE t1.u_from = 123 AND t2.u_to <> 123 
		AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

		UNION ALL
		SELECT t2.u_to AS friend, t2.u_from AS friend_of_friend
		FROM t AS t1   
		INNER JOIN t AS t2 ON t1.u_to = t2.u_to 
		WHERE t1.u_from = 123 AND t2.u_from <> 123 
		AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

		UNION ALL
		SELECT t2.u_from AS friend, t2.u_to AS friend_of_friend
		FROM t AS t1   
		INNER JOIN t AS t2 ON t1.u_from = t2.u_from  
		WHERE t1.u_to = 123 AND t2.u_to <> 123 
		AND t2.u_to NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123)

		UNION ALL
		SELECT t2.u_to AS friend, t2.u_from AS friend_of_friend
		FROM t AS t1   
		INNER JOIN t AS t2 ON t1.u_from = t2.u_to  
		WHERE t1.u_to = 123 AND t2.u_from <> 123 
		AND t2.u_from NOT IN (SELECT u_to FROM t WHERE u_from = 123 UNION SELECT u_from FROM t WHERE u_to = 123) 
	) AS top_table
	GROUP BY friend, friend_of_friend
) AS top_table_distinct_friends
GROUP BY friend_of_friend
ORDER BY common_connexions DESC;

-- we get:
-- friend_of_friend  common_connexions 
-- 321               3
-- 456               2
-- 318               1
-- 387               1
-- 341               1
